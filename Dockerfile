FROM node:10

RUN mkdir -p /var/www/app

COPY ./node/package*.json /var/www/app/
COPY ./node/ /var/www/app/
COPY ./ops-tools/run.sh /run.sh

RUN chmod +x /run.sh

WORKDIR /var/www/app/node

RUN npm i -g nodemon

ENTRYPOINT ["/run.sh"]
