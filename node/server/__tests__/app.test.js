const request = require('supertest');
const app = require('../src/app');

afterAll(async () => {
  await app.close();
});

test('teams endpoint is working', async () => {
  const response = await request(app).get('/api/teams');
  expect(response.status).toBe(200);
  expect(response.type).toEqual('application/json');
});

test('matches endpoint is working', async () => {
  const response = await request(app).get('/api/matches');
  expect(response.status).toBe(200);
  expect(response.type).toEqual('application/json');
});
