'use strict';

const fs = require('fs');
const mysql = require('promise-mysql');

const config = require('../config/config.js');

const db = {
  exec(sql, values) {
    return new Promise((resolve, reject) => {
      mysql.createConnection({
        host: config.host,
        user: config.mysql_user,
        password: config.mysql_password,
        database: config.mysql_database
      }).then(con => {
        const results = con.query(sql, values);
        con.end();
        return results;
      }).then(results => {
        if (!Array.isArray(results)) {
          results = [results];
        }
        resolve(results);
      }).catch(err => {
        reject(err);
      });
    });
  },

  execFile(filename) {
    fs.readFile(filename, 'utf8', (err, data) => {
      if (err) {
        throw err;
      }
      this.exec(data);
    });
    return this;
  }
};

module.exports = db;
