'use strict';

const db = require('../db');

module.exports = {
  getPlayers(teamId) {
    return db.exec('SELECT * FROM `players` WHERE team_id=?', [teamId]);
  },

  getPlayer(id) {
    return db.exec('SELECT * FROM `players` WHERE id=?', [id]).then(results => {
      return results[0];
    });
  },

  updatePlayer(player) {
    return db.exec('UPDATE `players` SET goals_scored=?, assists=?, goals_own=?, reds=?, yellows=? WHERE id=?',
      [player.goals_scored, player.assists, player.goals_own, player.reds, player.yellows, player.id]);
  }
};
