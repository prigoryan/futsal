'use strict';

const db = require('../db');
const playerDao = require('./PlayerDao');

module.exports = {
  getTeams() {
    return db.exec('SELECT * FROM `teams` ORDER BY points DESC').then(async results => {
      return results;
    });
  },

  getTeam(id) {
    return db.exec('SELECT * FROM `teams` WHERE id=?', [id]).then(async results => {
      const team = results[0];
      team.players = await playerDao.getPlayers(id);
      return team;
    });
  },

  updateTeam(team) {
    return db.exec('UPDATE `teams` SET games=?, wins=?, losses=?, draws=?, goals_for=?, goals_against=?, diff=?, points=? WHERE id=?',
      [team.games, team.wins, team.losses, team.draws, team.goals_for, team.goals_against, team.diff, team.points, team.id]);
  }
};
