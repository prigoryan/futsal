'use strict';

const db = require('../db');
const teamDao = require('./TeamDao');
const eventDao = require('./EventDao');

module.exports = {
  getMatches() {
    return db.exec('SELECT * FROM `matches` ORDER BY state').then(async results => {
      const matches = results;
      const teams = await teamDao.getTeams();
      for (let i = 0; i < matches.length; i++) {
        matches[i].home_team = teams.find(team => team.id === matches[i].home_team);
        matches[i].away_team = teams.find(team => team.id === matches[i].away_team);
      }
      return matches;
    });
  },

  getMatch(id) {
    return db.exec('SELECT * FROM `matches` WHERE id=?', [id]).then(async results => {
      const match = results[0];
      [match.home_team, match.away_team, match.events] = await Promise.all([
        teamDao.getTeam(match.home_team),
        teamDao.getTeam(match.away_team),
        eventDao.getEvents(match.id)
      ]);
      return match;
    });
  },

  updateMatch(match) {
    return db.exec('UPDATE `matches` SET goals_home=?, goals_away=? WHERE id=?',
      [match.goals_home, match.goals_away, match.id]);
  },

  startMatch(id) {
    return db.exec('UPDATE `matches` SET state=\'ONGOING\' WHERE id=?',
      [id]);
  },

  endMatch(id) {
    return db.exec('UPDATE `matches` SET state=\'OVER\' WHERE id=?',
      [id]);
  }
};
