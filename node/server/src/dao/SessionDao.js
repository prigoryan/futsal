'use strict';

const db = require('../db');

module.exports = {
  getSession(token) {
    return db.exec('SELECT * FROM `sessions` WHERE token=?', [token]).then(results => {
      return results[0];
    });
  },

  addSession(login, token) {
    return db.exec('INSERT INTO `sessions` VALUES (?, ?)', [token, login]);
  },

  deleteSessionByToken(token) {
    return db.exec('DELETE FROM `sessions` WHERE token=?', [token]);
  }
};
