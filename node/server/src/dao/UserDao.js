'use strict';

const db = require('../db');

module.exports = {
  getUserByLogin(login) {
    return db.exec('SELECT * FROM `users` WHERE login=?', [login]).then(results => {
      return results[0];
    });
  }
};
