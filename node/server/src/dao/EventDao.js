'use strict';

const db = require('../db');
const playerDao = require('./PlayerDao');

module.exports = {
  getEvents(matchId) {
    return db.exec('SELECT * FROM `events` WHERE match_id=? ORDER BY time ASC', [matchId]).then(async results => {
      const events = results;
      for (let i = 0; i < events.length; i++) {
        [events[i].player1, events[i].player2] = await Promise.all([
          playerDao.getPlayer(events[i].player1_id),
          playerDao.getPlayer(events[i].player2_id)
        ]);
      }
      return results;
    });
  },

  addEvent(event) {
    return db.exec('INSERT INTO `events` VALUES (null, ?, ?, ?, ?, ?, ?)',
      [event.match_id, event.team, event.player1_id, event.player2_id, event.time, event.type]);
  }
};
