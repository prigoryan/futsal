'use strict';

const eventDao = require('../dao/EventDao');
const matchDao = require('../dao/MatchDao');
const teamDao = require('../dao/TeamDao');
const playerDao = require('../dao/PlayerDao');
const logger = require('../service/Logger');

module.exports = {
  async addGoal(event) {
    const [match, player1, player2] = await Promise.all([
      matchDao.getMatch(event.match_id),
      playerDao.getPlayer(event.player1_id),
      playerDao.getPlayer(event.player2_id)
    ]);
    const [teamScored, teamMissed] = await Promise.all([
      teamDao.getTeam(event.team === 'HOME' ? match.home_team.id : match.away_team.id),
      teamDao.getTeam(event.team === 'AWAY' ? match.home_team.id : match.away_team.id)
    ]);

    event.team === 'HOME' ? match.goals_home++ : match.goals_away++;
    teamScored.goals_for++;
    teamScored.diff++;
    teamMissed.goals_against++;
    teamMissed.diff--;
    player1.goals_scored++;
    if (player2) {
      player2.assists++;
    }
    if (player2) {
      await Promise.all([
        eventDao.addEvent(event),
        matchDao.updateMatch(match),
        teamDao.updateTeam(teamScored),
        teamDao.updateTeam(teamMissed),
        playerDao.updatePlayer(player1),
        playerDao.updatePlayer(player2)
      ])
        .catch(err => {
          logger.error(err);
        });
    } else {
      await Promise.all([
        eventDao.addEvent(event),
        matchDao.updateMatch(match),
        teamDao.updateTeam(teamScored),
        teamDao.updateTeam(teamMissed),
        playerDao.updatePlayer(player1)
      ])
        .catch(err => {
          logger.error(err);
        });
    }
  },

  async addOwnGoal(event) {
    const [match, player1] = await Promise.all([
      matchDao.getMatch(event.match_id),
      playerDao.getPlayer(event.player1_id)
    ]);
    const [teamScored, otherTeam] = await Promise.all([
      teamDao.getTeam(event.team === 'AWAY' ? match.home_team.id : match.away_team.id),
      teamDao.getTeam(event.team === 'HOME' ? match.home_team.id : match.away_team.id)
    ]);

    event.team === 'AWAY' ? match.goals_home++ : match.goals_away++;
    teamScored.goals_against++;
    teamScored.diff--;
    otherTeam.goals_for++;
    otherTeam.diff++;
    player1.goals_own++;
    await Promise.all([
      eventDao.addEvent(event),
      matchDao.updateMatch(match),
      teamDao.updateTeam(teamScored),
      teamDao.updateTeam(otherTeam),
      playerDao.updatePlayer(player1)
    ])
      .catch(err => {
        logger.error(err);
      });
  },

  async addRedCard(event) {
    const player = await playerDao.getPlayer(event.player1_id);
    player.reds++;
    await Promise.all([
      eventDao.addEvent(event),
      playerDao.updatePlayer(player)
    ])
      .catch(err => {
        logger.error(err);
      });
  },

  async addYellowCard(event) {
    const player = await playerDao.getPlayer(event.player1_id);
    player.yellows++;
    await Promise.all([
      eventDao.addEvent(event),
      playerDao.updatePlayer(player)
    ])
      .catch(err => {
        logger.error(err);
      });
  }
};
