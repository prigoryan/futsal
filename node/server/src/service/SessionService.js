'use strict';

const sessionDao = require('../dao/SessionDao');
const userDao = require('../dao/UserDao');

module.exports = {
  async verifyRole(token, role) {
    const session = await sessionDao.getSession(token);
    if (!session) {
      return false;
    }
    const user = await userDao.getUserByLogin(session.user_login);
    if (!user || user.role !== role) {
      return false;
    }
    return true;
  }
};
