'use strict';

const path = require('path');
const Koa = require('koa');
const serve = require('koa-static');
const bodyParser = require('koa-bodyparser');
const koaLogger = require('koa-logger');
const cors = require('@koa/cors');

process.env.NODE_ENV = 'development';
const config = require('../config/config.js');

const logger = require('./service/Logger');
const matchEndpoint = require('./endpoint/MatchEndpoint');
const teamEndpoint = require('./endpoint/TeamEndpoint');
const sessionEndpoint = require('./endpoint/SessionEndpoint');
const eventEndpoint = require('./endpoint/EventEndpoint');
const app = new Koa();

process.on('uncaughtException', err => {
  logger.error(err);
  process.exit(1);
});

process.on('unhandledRejection', (reason, p) => {
  logger.error(reason, p);
  process.exit(1);
});

app.use(koaLogger());
app.use(bodyParser());
app.use(cors());
app.use(serve(path.join(__dirname, '/../public')));

app.use(matchEndpoint.routes());
app.use(teamEndpoint.routes());
app.use(sessionEndpoint.routes());
app.use(eventEndpoint.routes());

const server = app.listen(config.node_port, () => {
  console.log(`Listening on port ${config.node_port}`);
});
module.exports = server;
