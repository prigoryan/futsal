'use strict';

const router = require('koa-router')();
const Password = require('password-hash-and-salt');
const uuidv4 = require('uuid/v4');
const userDao = require('../dao/UserDao');
const sessionDao = require('../dao/SessionDao');
const logger = require('../service/Logger');

router.post('/api/session', async ctx => {
  const login = ctx.request.body.login;
  const password = ctx.request.body.password;
  const user = await userDao.getUserByLogin(login);
  if (!user) {
    ctx.throw(401, 'Invalid combination of credentials.');
  }
  await new Promise((resolve, reject) => {
    Password(password).verifyAgainst(user.password, async (err, verified) => {
      if (err || !verified) {
        reject(err);
      }
      resolve();
    });
  }).catch(() => ctx.throw(401, 'Invalid combination of credentials.'));
  const token = uuidv4();
  await sessionDao.addSession(login, token)
    .catch(err => {
      logger.error(err);
      ctx.throw(500);
    });
  ctx.body = user.login + ':' + user.role + ':' + token; // probably should be passed as a cookie, but CORS wouldn't let me.
  ctx.status = 200;
});

router.delete('/api/session', async ctx => {
  const token = ctx.request.body.token;
  await sessionDao.deleteSessionByToken(token)
    .catch(err => {
      logger.error(err);
      ctx.throw(500);
    });
  ctx.status = 200;
});

module.exports = router;
