'use strict';

const router = require('koa-router')();
const sessionService = require('../service/SessionService');
const eventService = require('../service/EventService');
const logger = require('../service/Logger');

router.post('/api/events', async ctx => {
  const event = ctx.request.body.event;
  const token = ctx.request.body.token;
  const authorized = await sessionService.verifyRole(token, 'editor');
  if (!authorized) {
    ctx.throw(403);
  }
  switch (event.type) {
    case 'GOAL':
      await eventService.addGoal(event).catch(err => {
        logger.error(err);
        ctx.throw(500);
      });
      break;
    case 'OWN_GOAL':
      await eventService.addOwnGoal(event).catch(err => {
        logger.error(err);
        ctx.throw(500);
      });
      break;
    case 'RED':
      await eventService.addRedCard(event).catch(err => {
        logger.error(err);
        ctx.throw(500);
      });
      break;
    case 'YELLOW':
      await eventService.addYellowCard(event).catch(err => {
        logger.error(err);
        ctx.throw(500);
      });
      break;
  }
  ctx.status = 200;
});

module.exports = router;
