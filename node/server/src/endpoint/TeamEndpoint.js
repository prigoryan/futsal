'use strict';

const router = require('koa-router')();
const teamDao = require('../dao/TeamDao');
const logger = require('../service/Logger');

router.get('/api/teams', async ctx => {
  ctx.body = await teamDao.getTeams()
    .catch(err => {
      logger.error(err);
      ctx.throw(500);
    });
  ctx.status = 200;
});

router.get('/api/team/:id', async ctx => {
  const id = ctx.params.id;
  ctx.body = await teamDao.getTeam(id)
    .catch(err => {
      logger.error(err);
      ctx.throw(500);
    });
});

module.exports = router;
