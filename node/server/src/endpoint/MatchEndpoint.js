'use strict';

const router = require('koa-router')();
const matchDao = require('../dao/MatchDao');
const teamDao = require('../dao/TeamDao');
const sessionService = require('../service/SessionService');
const logger = require('../service/Logger');

router.get('/api/matches', async ctx => {
  ctx.body = await matchDao.getMatches()
    .catch(err => {
      logger.error(err);
      ctx.throw(500);
    });
  ctx.status = 200;
});

router.get('/api/match/:id', async ctx => {
  const id = ctx.params.id;
  ctx.body = await matchDao.getMatch(id)
    .catch(err => {
      logger.error(err);
      ctx.throw(500);
    });
  ctx.status = 200;
});

router.post('/api/match/start', async ctx => {
  const id = ctx.request.body.id;
  const token = ctx.request.body.token;
  const authorized = await sessionService.verifyRole(token, 'editor');
  if (!authorized) {
    ctx.throw(403);
  }
  await matchDao.startMatch(id)
    .catch(err => {
      logger.error(err);
      ctx.throw(500);
    });
  ctx.status = 200;
});

router.post('/api/match/end', async ctx => {
  const id = ctx.request.body.id;
  const token = ctx.request.body.token;
  const authorized = await sessionService.verifyRole(token, 'editor');
  if (!authorized) {
    ctx.throw(403);
  }
  const match = await matchDao.getMatch(id);
  let [home, away] = [match.home_team, match.away_team];
  home.games++;
  away.games++;
  if (match.goals_home > match.goals_away) {
    home.wins++;
    away.losses++;
    home.points += 3;
  } else if (match.goals_home < match.goals_away) {
    away.wins++;
    home.losses++;
    away.points += 3;
  } else {
    home.draws++;
    away.draws++;
    home.points++;
    away.points++;
  }
  await Promise.all([
    matchDao.endMatch(id),
    teamDao.updateTeam(home),
    teamDao.updateTeam(away)
  ])
    .catch(err => {
      logger.error(err);
      ctx.throw(500);
    });
  ctx.status = 200;
});

module.exports = router;
