'use strict';

const axios = require('axios');
const fs = require('fs');

const getTeam = require('./team-name-generator');

const DB_NAME = 'futsal';
const TEAMS = 10; // even number
const PLAYERS_PER_TEAM = 15;
const ROUNDS = 2;
const GENDER = 'male';
const REGION = 'united states';

function randRange(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

async function randNames(gender, region, amount) {
  const people = await axios.get(`https://uinames.com/api?gender=${gender}&region=${region}&amount=${amount}`)
    .then(response => {
      return response.data;
    });
  return people.map(person => person.name + ' ' + person.surname);
}

function generateData(names) {
  let data = {
    teams: [],
    players: [],
    matches: [],
    events: []
  };

  for (let i = 1; i <= TEAMS; i++) {
    data.teams.push({
      name: getTeam(),
      games: 0,
      wins: 0,
      losses: 0,
      draws: 0,
      goals_for: 0,
      goals_against: 0,
      diff: 0,
      points: 0
    });
    for (let j = 0; j < PLAYERS_PER_TEAM; j++) {
      data.players.push({
        name: names[(i - 1) * PLAYERS_PER_TEAM + j],
        team_id: i,
        shots: 0,
        shots_on_target: 0,
        goals_scored: 0,
        penalties: 0,
        penalties_scored: 0,
        goals_own: 0,
        assists: 0,
        saves: 0,
        yellows: 0,
        reds: 0
      });
    }
  }

  data = addMatches(data, ROUNDS);
  data = simulateMatches(data);

  return data;
}

function addMatches(data, rounds) {
  let brackets = [[], []]; // https://stackoverflow.com/questions/6648512/scheduling-algorithm-for-a-round-robin-tournament
  for (let i = 0; i < data.teams.length / 2; i++) {
    brackets[0][i] = i + 1;
    brackets[1][i] = data.teams.length - i;
  }

  for (let i = 0; i < rounds; i++) {
    for (let j = 0; j < brackets[0].length; j++) {
      data.matches.push({
        home_team: brackets[0][j],
        away_team: brackets[1][j],
        state: i === rounds - 1 ? 'ONGOING' : 'OVER', // last round matches are ongoing
        goals_home: 0,
        goals_away: 0
      });
    }

    // see previous comment
    const elem1 = brackets[0].pop();
    brackets[1].push(elem1);
    const elem2 = brackets[1].shift();
    brackets[0].splice(1, 0, elem2);
  }

  return data;
}

function simulateMatches(data) {
  for (let i = 1; i <= data.matches.length; i++) {
    const match = data.matches[i - 1];
    const goals = randRange(1, 7);
    const ownGoals = Math.random() > 0.9 ? 1 : 0;
    const reds = randRange(0, 1);
    const yellows = randRange(1, 3);

    for (let j = 0; j < goals; j++) {
      const team = Math.random() > 0.5 ? 'HOME' : 'AWAY';
      const teamId = team === 'HOME' ? match.home_team : match.away_team;
      const oppositeTeamId = team === 'AWAY' ? match.home_team : match.away_team;
      const firstPlayerId = (teamId - 1) * PLAYERS_PER_TEAM + 1;
      const scoringPlayer = randRange(firstPlayerId, firstPlayerId + PLAYERS_PER_TEAM - 1);
      const assistingPlayer = Math.random() > 0.2 ? randRange(firstPlayerId, firstPlayerId + PLAYERS_PER_TEAM - 1) : null;
      data.events.push({
        match_id: i,
        team,
        player1_id: scoringPlayer,
        player2_id: assistingPlayer,
        time: randRange(1, 2400),
        type: 'GOAL'
      });
      if (team === 'HOME') {
        match.goals_home++;
      } else {
        match.goals_away++;
      }
      data.teams[teamId - 1].goals_for++;
      data.teams[teamId - 1].diff++;
      data.teams[oppositeTeamId - 1].goals_against++;
      data.teams[oppositeTeamId - 1].diff--;
      data.players[scoringPlayer - 1].shots++;
      data.players[scoringPlayer - 1].shots_on_target++;
      data.players[scoringPlayer - 1].goals_scored++;
      if (assistingPlayer) {
        data.players[assistingPlayer - 1].assists++;
      }
    }
    for (let j = 0; j < ownGoals; j++) {
      const team = Math.random() > 0.5 ? 'HOME' : 'AWAY';
      const teamId = team === 'HOME' ? match.home_team : match.away_team;
      const oppositeTeamId = team === 'AWAY' ? match.home_team : match.away_team;
      const firstPlayerId = (teamId - 1) * PLAYERS_PER_TEAM + 1;
      const scoringPlayer = randRange(firstPlayerId, firstPlayerId + PLAYERS_PER_TEAM - 1);
      data.events.push({
        match_id: i,
        team,
        player1_id: scoringPlayer,
        player2_id: null,
        time: randRange(1, 2400),
        type: 'OWN_GOAL'
      });
      if (team === 'AWAY') {
        match.goals_home++;
      } else {
        match.goals_away++;
      }
      data.teams[teamId - 1].goals_against++;
      data.teams[teamId - 1].diff--;
      data.teams[oppositeTeamId - 1].goals_for++;
      data.teams[oppositeTeamId - 1].diff++;
      data.players[scoringPlayer - 1].goals_own++;
    }
    for (let j = 0; j < reds; j++) {
      const team = Math.random() > 0.5 ? 'HOME' : 'AWAY';
      const firstPlayerId = team === 'HOME' ? (match.home_team - 1) * PLAYERS_PER_TEAM + 1
        : (match.away_team - 1) * PLAYERS_PER_TEAM + 1;
      const scoringPlayer = randRange(firstPlayerId, firstPlayerId + PLAYERS_PER_TEAM - 1);
      data.events.push({
        match_id: i,
        team,
        player1_id: scoringPlayer,
        player2_id: null,
        time: randRange(1, 2400),
        type: 'RED'
      });
      data.players[scoringPlayer - 1].reds++;
    }
    for (let j = 0; j < yellows; j++) {
      const team = Math.random() > 0.5 ? 'HOME' : 'AWAY';
      const firstPlayerId = team === 'HOME' ? (match.home_team - 1) * PLAYERS_PER_TEAM + 1
        : (match.away_team - 1) * PLAYERS_PER_TEAM + 1;
      const scoringPlayer = randRange(firstPlayerId, firstPlayerId + PLAYERS_PER_TEAM - 1);
      data.events.push({
        match_id: i,
        team,
        player1_id: scoringPlayer,
        player2_id: null,
        time: randRange(1, 2400),
        type: 'YELLOW'
      });
      data.players[scoringPlayer - 1].yellows++;
    }
    if (match.state === 'OVER') {
      const home = data.teams[match.home_team - 1];
      const away = data.teams[match.away_team - 1];
      home.games++;
      away.games++;
      if (match.goals_home > match.goals_away) {
        home.wins++;
        away.losses++;
        home.points += 3;
      } else if (match.goals_home < match.goals_away) {
        away.wins++;
        home.losses++;
        away.points += 3;
      } else {
        home.draws++;
        away.draws++;
        home.points++;
        away.points++;
      }
    }
  }

  return data;
}

function dataToSql(data) {
  let script = `USE \`${DB_NAME}\`;\n`;
  for (let i = 0; i < data.teams.length; i++) {
    const team = data.teams[i];
    script += `INSERT INTO \`teams\` VALUES (null, '${team.name}', ${team.games}, ${team.wins}, ${team.losses},` +
      ` ${team.draws}, ${team.goals_for}, ${team.goals_against}, ${team.diff}, ${team.points});\n`;
  }
  for (let i = 0; i < data.players.length; i++) {
    const player = data.players[i];
    script += `INSERT INTO \`players\` VALUES (null, '${player.name}', ${player.team_id}, ${player.shots},` +
      ` ${player.shots_on_target}, ${player.goals_scored}, ${player.penalties}, ${player.penalties_scored},` +
      ` ${player.goals_own}, ${player.assists}, ${player.saves}, ${player.yellows}, ${player.reds});\n`;
  }
  for (let i = 0; i < data.matches.length; i++) {
    const match = data.matches[i];
    script += `INSERT INTO \`matches\` VALUES (null, ${match.home_team}, ${match.away_team}, '${match.state}', ${match.goals_home},` +
      `${match.goals_away});\n`;
  }
  for (let i = 0; i < data.events.length; i++) {
    const event = data.events[i];
    script += `INSERT INTO \`events\` VALUES (null, ${event.match_id}, '${event.team}', ${event.player1_id}, ${event.player2_id},` +
      ` ${event.time}, '${event.type}');\n`;
  }
  return script;
}

randNames(GENDER, REGION, TEAMS * PLAYERS_PER_TEAM).then(names => {
  const data = generateData(names);
  const script = dataToSql(data);
  fs.writeFileSync('./node/server/test-data-generator/data.sql', script);
}).catch(err => console.log(err));
