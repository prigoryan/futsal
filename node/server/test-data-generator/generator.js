'use strict';

const axios = require('axios');
const fs = require('fs');

const getTeam = require('./team-name-generator');

const DB_NAME = 'futsal';
const TEAMS = 10;
const PLAYERS_PER_TEAM = 15;
const MATCHES = 100;
const GENDER = 'male';
const REGION = 'united states';
const MATCH_STATES = ['SCHEDULED', 'ONGOING', 'OVER'];

function randRange(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

async function randNames(gender, region, amount) {
  const people = await axios.get(`https://uinames.com/api?gender=${gender}&region=${region}&amount=${amount}`)
    .then(response => {
      return response.data;
    });
  return people.map(person => person.name + ' ' + person.surname);
}

function addTeams(script, names) {
  for (let i = 0; i < TEAMS; i++) {
    const name = getTeam();
    const games = randRange(5, 30);
    const wins = randRange(0, games);
    const losses = randRange(0, games - wins);
    const draws = games - wins - losses;
    const goalsFor = randRange(0, games * 5);
    const goalsAgainst = randRange(0, games * 5);
    const diff = goalsFor - goalsAgainst;
    const points = 3 * wins + draws;

    script += `\nINSERT INTO \`teams\` VALUES (null, '${name}', ${games}, ${wins}, ${losses}, ${draws}, ${goalsFor}, ${goalsAgainst}, ${diff}, ${points});\n`;
    script = addPlayers(script, i, games, names);
  }
  return script;
}

function addPlayers(script, team, games, names) {
  for (let j = 0; j < PLAYERS_PER_TEAM; j++) {
    const name = names[team * PLAYERS_PER_TEAM + j];
    const teamId = team + 1;
    const shots = randRange(0, games);
    const shotsOnTarget = randRange(0, shots);
    const goalsScored = randRange(0, shotsOnTarget);
    const penalties = randRange(0, games / 2);
    const penaltiesScored = randRange(0, penalties);
    const goalsOwn = randRange(0, games / 8);
    const assists = randRange(0, games);
    const saves = randRange(0, games);
    const yellows = randRange(0, games / 2);
    const reds = randRange(0, games / 2);

    script += `INSERT INTO \`players\` VALUES (null, '${name}', ${teamId}, ${shots}, ${shotsOnTarget}, ${goalsScored}, ${penalties}, ${penaltiesScored}, ${goalsOwn}, ${assists}, ${saves}, ${yellows}, ${reds});\n`;
  }
  return script;
}

function addMatches(script) {
  for (let i = 0; i < MATCHES; i++) {
    const home = randRange(1, TEAMS);
    let away = randRange(1, TEAMS);
    if (away === home) {
      home === TEAMS ? away-- : away++;
    }
    let state = MATCH_STATES[randRange(1, 2)];
    let goalsHome = 0;
    let goalsAway = 0;
    if (Math.random() > 0.9) {
      state = 'SCHEDULED';
    } else {
      goalsHome = randRange(0, 7);
      goalsAway = randRange(0, 7);
    }

    script += `\nINSERT INTO \`matches\` VALUES (null, ${home}, ${away}, '${state}', ${goalsHome}, ${goalsAway});\n`;
    script = addEvents(script, i, home, away, goalsHome, goalsAway, state);
  }
  return script;
}

function addEvents(script, match, home, away, goalsHome, goalsAway, state) {
  script = addGoals(script, match, home, away, goalsHome, goalsAway);
  if (state !== 'SCHEDULED') {
    script = addCards(script, match, home, away);
  }
  return script;
}

function addGoals(script, match, home, away, goalsHome, goalsAway) {
  for (let j = 0; j < goalsHome; j++) {
    const own = Math.random() > 0.95;
    const matchId = match + 1;
    let team = 'HOME';
    const time = randRange(0, 40 * 60);
    let type = 'GOAL';
    let player1 = randRange((home - 1) * PLAYERS_PER_TEAM, home * PLAYERS_PER_TEAM - 1) + 1;
    let player2 = Math.random() > 0.8 ? null : randRange((home - 1) * PLAYERS_PER_TEAM, home * PLAYERS_PER_TEAM - 1) + 1;
    if (player2 && player2 === player1) {
      player2 === home * PLAYERS_PER_TEAM ? player2-- : player2++;
    }
    if (own) {
      team = 'AWAY';
      type = 'OWN_GOAL';
      player1 = randRange((away - 1) * PLAYERS_PER_TEAM, away * PLAYERS_PER_TEAM - 1) + 1;
      player2 = null;
    }

    script += `INSERT INTO \`events\` VALUES (null, ${matchId}, '${team}', ${player1}, ${player2}, ${time}, '${type}');\n`;
  }
  const matchId = match + 1;
  for (let j = 0; j < goalsAway; j++) {
    const own = Math.random() > 0.95;
    let team = 'AWAY';
    const time = randRange(0, 40 * 60);
    let type = 'GOAL';
    let player1 = randRange((away - 1) * PLAYERS_PER_TEAM, away * PLAYERS_PER_TEAM - 1) + 1;
    let player2 = Math.random() > 0.8 ? null : randRange((away - 1) * PLAYERS_PER_TEAM, away * PLAYERS_PER_TEAM - 1) + 1;
    if (player2 && player2 === player1) {
      player2 === away * PLAYERS_PER_TEAM ? player2-- : player2++;
    }
    if (own) {
      team = 'HOME';
      type = 'OWN_GOAL';
      player1 = randRange((home - 1) * PLAYERS_PER_TEAM, home * PLAYERS_PER_TEAM - 1) + 1;
      player2 = null;
    }

    script += `INSERT INTO \`events\` VALUES (null, ${matchId}, '${team}', ${player1}, ${player2}, ${time}, '${type}');\n`;
  }
  return script;
}

function addCards(script, match, home, away) {
  const matchId = match + 1;
  const yellowsHome = randRange(0, 2);
  const redsHome = randRange(0, 1);
  const yellowsAway = randRange(0, 2);
  const redsAway = randRange(0, 1);

  for (let j = 0; j < yellowsHome; j++) {
    const team = 'HOME';
    const player1 = randRange((home - 1) * PLAYERS_PER_TEAM, home * PLAYERS_PER_TEAM - 1) + 1;
    const player2 = null;
    const time = randRange(0, 40 * 60);
    const type = 'YELLOW';

    script += `INSERT INTO \`events\` VALUES (null, ${matchId}, '${team}', ${player1}, ${player2}, ${time}, '${type}');\n`;
  }
  for (let j = 0; j < redsHome; j++) {
    const team = 'HOME';
    const player1 = randRange((home - 1) * PLAYERS_PER_TEAM, home * PLAYERS_PER_TEAM - 1) + 1;
    const player2 = null;
    const time = randRange(0, 40 * 60);
    const type = 'RED';

    script += `INSERT INTO \`events\` VALUES (null, ${matchId}, '${team}', ${player1}, ${player2}, ${time}, '${type}');\n`;
  }
  for (let j = 0; j < yellowsAway; j++) {
    const team = 'AWAY';
    const player1 = randRange((away - 1) * PLAYERS_PER_TEAM, away * PLAYERS_PER_TEAM - 1) + 1;
    const player2 = null;
    const time = randRange(0, 40 * 60);
    const type = 'YELLOW';

    script += `INSERT INTO \`events\` VALUES (null, ${matchId}, '${team}', ${player1}, ${player2}, ${time}, '${type}');\n`;
  }
  for (let j = 0; j < redsAway; j++) {
    const team = 'AWAY';
    const player1 = randRange((away - 1) * PLAYERS_PER_TEAM, away * PLAYERS_PER_TEAM - 1) + 1;
    const player2 = null;
    const time = randRange(0, 40 * 60);
    const type = 'RED';

    script += `INSERT INTO \`events\` VALUES (null, ${matchId}, '${team}', ${player1}, ${player2}, ${time}, '${type}');\n`;
  }
  return script;
}

let script = `USE \`${DB_NAME}\`;\n`;

randNames(GENDER, REGION, TEAMS * PLAYERS_PER_TEAM).then(names => {
  script = addTeams(script, names);
  script = addMatches(script);
  fs.writeFileSync('./node/server/test-data-generator/data.sql', script);
}).catch(err => console.log(err));
