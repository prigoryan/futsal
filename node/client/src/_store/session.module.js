import Vue from 'vue'
import VueCookies from 'vue-cookies'

import SessionApi from '../api/SessionApi'

Vue.use(VueCookies)

const cookie = VueCookies.get('session')

let initialState = {
  user: '',
  role: '',
  token: '',
  cookie: ''
}

if (cookie) {
  const [user, role, token] = cookie.split(':')
  initialState = {
    user,
    role,
    token,
    cookie
  }
}

export const session = {
  namespaced: true,
  state: initialState,
  mutations: {
    setUser (state, user) {
      state.user = user
    },
    setRole (state, role) {
      state.role = role
    },
    setToken (state, token) {
      state.token = token
    },
    setCookie (state, cookie) {
      state.cookie = cookie
    }
  },
  actions: {
    async logIn (context, { login, password }) {
      const response = await SessionApi.logIn(login, password)
      if (response.status === 200) {
        const [user, role, token] = response.data.split(':')
        context.commit('setUser', user)
        context.commit('setRole', role)
        context.commit('setToken', token)
        context.commit('setCookie', response.data)
        return response.data
      } else {
        throw new Error(401)
      }
    },
    async logOut (context) {
      const response = await SessionApi.logOut(context.state.token)
      context.commit('setUser', '')
      context.commit('setRole', '')
      context.commit('setToken', '')
      context.commit('setCookie', '')
      if (response.status === 200) {
        return 200
      } else {
        throw new Error(400)
      }
    },
    async verifyRole (context, { roles }) {
      console.log(context.state.role + ' should be in [' + roles + ']')
      return true // TODO verify session server-side
    }
  }
}
