import Vue from 'vue'
import Vuex from 'vuex'

import { session } from './session.module'
import { info } from './info.module'

Vue.use(Vuex)

export const store = new Vuex.Store({
  modules: {
    info,
    session
  }
})
