import teamApi from '../api/TeamApi'
import matchApi from '../api/MatchApi'

export const info = {
  namespaced: true,
  state: {
    teams: [],
    matches: [],
    team: {},
    match: {}
  },
  mutations: {
    setTeams (state, teams) {
      state.teams = teams
    },
    setMatches (state, matches) {
      state.matches = matches
    },
    setTeam (state, team) {
      state.team = team
    },
    setMatch (state, match) {
      state.match = match
    }
  },
  actions: {
    async refreshTeams (context) {
      const teams = await teamApi.getTeams()
      context.commit('setTeams', teams)
    },
    async refreshMatches (context) {
      const matches = await matchApi.getMatches()
      context.commit('setMatches', matches)
    },
    async getTeam (context, { id }) {
      context.commit('setTeam', {})
      const team = await teamApi.getTeam(id)
      context.commit('setTeam', team)
    },
    async getMatch (context, { id }) {
      context.commit('setMatch', {})
      const match = await matchApi.getMatch(id)
      context.commit('setMatch', match)
    }
  }
}
