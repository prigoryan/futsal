// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'

import { store } from './_store'
/* eslint-disable no-unused-vars */
import { fontawesome, router } from './_helpers'
import App from './App'
import EventLog from './components/EventLog'

Vue.config.productionTip = false

Vue.component('event-log', EventLog)

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App)
})
