import axios from 'axios'
import config from '../../config/config'
import { store } from '../_store/index'

export default {
  addEvent (event) {
    let payload = {
      event,
      token: store.state.session.token
    }
    return axios.post(config.api + '/events', payload)
      .then(response => {
        return response.data
      })
  }
}
