import axios from 'axios'
import config from '../../config/config'

export default {
  logIn (login, password) {
    return axios.post(config.api + '/session', { login, password }, {
      validateStatus (status) {
        return status >= 200 && status < 500
      }
    })
      .then(response => {
        return response
      })
  },
  logOut (token) {
    return axios.delete(config.api + '/session', {
      data: {
        token
      },
      validateStatus (status) {
        return status >= 200 && status < 500
      }
    })
      .then(response => {
        return response
      })
  }
}
