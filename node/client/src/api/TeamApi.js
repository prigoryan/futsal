import axios from 'axios'
import config from '../../config/config'

export default {
  getTeams () {
    return axios.get(config.api + '/teams')
      .then(response => {
        return response.data
      })
  },
  getTeam (id) {
    return axios.get(config.api + '/team/' + id)
      .then(response => {
        return response.data
      })
  }
}
