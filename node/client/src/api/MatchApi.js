import axios from 'axios'
import config from '../../config/config'
import { store } from '../_store'

export default {
  getMatches () {
    return axios.get(config.api + '/matches')
      .then(response => {
        return response.data
      })
  },
  getMatch (id) {
    return axios.get(config.api + '/match/' + id)
      .then(response => {
        return response.data
      })
  },
  startMatch (id) {
    let payload = {
      id,
      token: store.state.session.token
    }
    return axios.post(config.api + '/match/start', payload)
      .then(response => {
        return response.data
      })
  },
  endMatch (id) {
    let payload = {
      id,
      token: store.state.session.token
    }
    return axios.post(config.api + '/match/end', payload)
      .then(response => {
        return response.data
      })
  }
}
