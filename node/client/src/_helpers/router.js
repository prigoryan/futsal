import Vue from 'vue'
import Router from 'vue-router'

import FrontPage from '../components/FrontPage'
import LoginPage from '../components/LoginPage'
import LogoutPage from '../components/LogoutPage'
import MatchPage from '../components/MatchPage'
import TeamPage from '../components/TeamPage'
import EditMatchPage from '../components/EditMatchPage'

import { store } from '../_store/index'

Vue.use(Router)

const protectedRoutes = {
  '/match/:id/edit': ['editor']
}

const routes = [
  { path: '/', component: FrontPage },
  { path: '/login', component: LoginPage },
  { path: '/logout', component: LogoutPage },
  { path: '/match/:id', component: MatchPage },
  { path: '/match/:id/edit', component: EditMatchPage },
  { path: '/team/:id', component: TeamPage }
]

const router = new Router({
  mode: 'hash',
  routes
})

router.beforeEach(async (to, from, next) => {
  const roles = protectedRoutes[to.matched[0].path]
  if (roles && (!roles.includes(store.state.session.role) ||
    !(await store.dispatch('session/verifyRole', { roles })))) {
    next('/error?code=403') // TODO error page
  }
  next()
})

export { router }
