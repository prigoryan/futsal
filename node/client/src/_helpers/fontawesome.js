import Vue from 'vue'

import { library } from '@fortawesome/fontawesome-svg-core'
import { faAngleDoubleLeft, faAngleDoubleRight, faFutbol } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

library.add(faAngleDoubleLeft, faAngleDoubleRight, faFutbol)
Vue.component('font-awesome-icon', FontAwesomeIcon)
