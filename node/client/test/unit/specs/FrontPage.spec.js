import Vue from 'vue'
import FrontPage from '../../../src/components/FrontPage'

describe('HelloWorld.vue', () => {
  it('has a beforeCreate hook', () => {
    expect(typeof FrontPage.beforeCreate).toBe('function')
  })
})
