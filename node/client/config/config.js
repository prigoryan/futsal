'use strict';

const config = require('./config.json');
const defaultConfig = config.development;
const environment = process.env.NODE_ENV || 'development';
const environmentConfig = config[environment];
let finalConfig = Object.assign(defaultConfig, environmentConfig);
Object.assign(finalConfig, {
  api: finalConfig.protocol + '://' + finalConfig.host + ':' + finalConfig.port + finalConfig.path
});

module.exports = finalConfig;
