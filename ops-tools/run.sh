#!/bin/sh

npm install --no-bin-links
if [ "$NODE_ENV" = "production" ] ; then
  npm run start
else
  npm run test
  npm run dev
fi
