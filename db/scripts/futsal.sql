#DB creation file

DROP DATABASE IF EXISTS `futsal`;
CREATE DATABASE `futsal`;
USE `futsal`;

CREATE TABLE `users`
(
  login    VARCHAR(32)             NOT NULL,
  password NVARCHAR(512) BINARY    NOT NULL,
  role     ENUM ('user', 'editor') NOT NULL,

  PRIMARY KEY (login)
);

CREATE TABLE `sessions`
(
  token      VARCHAR(64) NOT NULL,
  user_login VARCHAR(32) NOT NULL,

  PRIMARY KEY (token),
  FOREIGN KEY (user_login) REFERENCES `users` (login)
    ON DELETE CASCADE
);

CREATE TABLE `teams`
(
  id            INT           NOT NULL AUTO_INCREMENT,
  name          NVARCHAR(128) NOT NULL,
  games         INT           NOT NULL,
  wins          INT           NOT NULL,
  losses        INT           NOT NULL,
  draws         INT           NOT NULL,
  goals_for     INT           NOT NULL,
  goals_against INT           NOT NULL,
  diff          INT           NOT NULL,
  points        INT           NOT NULL,

  PRIMARY KEY (id)
);

CREATE TABLE `players`
(
  id               INT          NOT NULL AUTO_INCREMENT,
  name             NVARCHAR(64) NOT NULL,
  team_id          INT DEFAULT NULL,
  shots            INT          NOT NULL,
  shots_on_target  INT          NOT NULL,
  goals_scored     INT          NOT NULL,
  penalties        INT          NOT NULL,
  penalties_scored INT          NOT NULL,
  goals_own        INT          NOT NULL,
  assists          INT          NOT NULL,
  saves            INT          NOT NULL,
  yellows          INT          NOT NULL,
  reds             INT          NOT NULL,

  PRIMARY KEY (id),
  FOREIGN KEY (team_id) REFERENCES `teams` (id)
    ON DELETE SET NULL
);

CREATE TABLE `matches`
(
  id         INT                                   NOT NULL AUTO_INCREMENT,
  home_team  INT                                   NOT NULL,
  away_team  INT                                   NOT NULL,
  state      ENUM ('SCHEDULED', 'ONGOING', 'OVER') NOT NULL,
  goals_home INT                                   NOT NULL,
  goals_away INT                                   NOT NULL,

  PRIMARY KEY (id),
  FOREIGN KEY (home_team) REFERENCES `teams` (id)
    ON DELETE CASCADE,
  FOREIGN KEY (away_team) REFERENCES `teams` (id)
    ON DELETE CASCADE
);

CREATE TABLE `events`
(
  id         INT                                        NOT NULL AUTO_INCREMENT,
  match_id   INT                                        NOT NULL,
  team       ENUM ('HOME', 'AWAY')                      NOT NULL,
  player1_id INT DEFAULT NULL,
  player2_id INT DEFAULT NULL,
  time       INT                                        NOT NULL,
  type       ENUM ('GOAL', 'OWN_GOAL', 'YELLOW', 'RED') NOT NULL,

  PRIMARY KEY (id),
  FOREIGN KEY (match_id) REFERENCES `matches` (id)
    ON DELETE CASCADE,
  FOREIGN KEY (player1_id) REFERENCES `players` (id)
    ON DELETE SET NULL,
  FOREIGN KEY (player1_id) REFERENCES `players` (id)
    ON DELETE SET NULL
);
